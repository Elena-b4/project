<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;
    protected array $guarded = [];

    public function equipments()
    {
        return $this->belongsToMany(
            Equipment::class,
            'car_equipment',
            'car_id',
            'equipment_id'
        );
    }

    public function equipmentElements()
    {
        return $this->belongsToMany(
            EquipmentElement::class,
            'car_equipment_elements',
            'car_id',
            'equipment_element_id'
        );
    }


}
