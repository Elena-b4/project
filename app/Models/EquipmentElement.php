<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EquipmentElement extends Model
{
    use HasFactory;
    protected array $guarded = [];

    public function cars()
    {
        return $this->belongsToMany(
            Car::class,
            'car_equipment_elements',
            'equipment_element_id',
            'car_id',
        );
    }
}
