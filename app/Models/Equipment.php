<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Equipment extends Model
{
    use HasFactory;
    protected array $guarded = [];

    public function cars(): BelongsToMany
    {
        return $this->belongsToMany(
            Car::class,
            'car_equipment',
            'equipment_id',
            'car_id',
        );
    }
}
