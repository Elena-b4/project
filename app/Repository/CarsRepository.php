<?php
declare(strict_types=1);

namespace App\Repository;

use App\Models\Car;
use App\Models\Equipment;
use App\Models\EquipmentElement;

class CarsRepository
{
    public function insertData(array $data)
    {
        $carsIdsForAdding = [];
        foreach ($data as $item) {
            $equipments = $item['equipments'];
            unset($item['equipments']);
            $car = Car::updateOrCreate($item);

            if (isset($equipments)) {
                foreach ($equipments as $group) {
                    $groupId = $group['id'];
                    $groupName = $group['name'];
                    $equipment = Equipment::firstOrCreate([
                        'id' => $groupId,
                        'title' => $groupName,
                    ]);
                    $equipment->cars()->sync($car->id);
                    $children = $group->children();
                    foreach ($children as $child) {
                        $equipmentElement = EquipmentElement::firstOrCreate([
                            'id' => $child['id'],
                            'title' => $child->__toString(),
                            'equipment_id' => $equipment->id,
                        ]);
                        $equipmentElement->cars()->attach($car->id);
                    }
                }
            }

            $carsIdsForAdding[] = $car->id;
            $existedCarIds = Car::all()->pluck('id')->toArray();
            $carIdsForDelete = array_diff($existedCarIds, $carsIdsForAdding);
            Car::destroy($carIdsForDelete);
        }
    }
}