<?php

namespace App\Console\Commands;

use App\Service\ParseXmlService;
use Illuminate\Console\Command;
use App\Repository\CarsRepository;

class ParseXML extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:xml {filePath}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse xml-file';

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Exception
     */
    public function handle(CarsRepository $carsRepository, ParseXmlService $parseXmlService)
    {
        $filePath = $this->argument('filePath') ?? 'public/data.xml';

        // parse xmlStrong
        try {
            $xmlFileString = file_get_contents($filePath);
            $xmlObject = simplexml_load_string($xmlFileString);
        } catch (\Exception $ex) {
            return $this->error('Invalid file.');
        }

        if (!$xmlObject) {
            return $this->error('Invalid file.');
        }

        try {
            $data = $parseXmlService->modifyInputDataFromXmlObject($xmlObject);
            $carsRepository->insertData($data);
        } catch (\Exception $ex1) {
            return $this->error('Insert data was failed.');
        }

        return $this->info('Data was insert successful!');
    }
}
