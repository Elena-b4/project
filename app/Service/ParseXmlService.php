<?php
declare(strict_types=1);

namespace App\Service;

use App\Models\BodyConfiguration;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Dealer;
use App\Models\Generation;
use App\Models\ModelCar;
use App\Models\Modification;
use DateTime;
use SimpleXMLElement;

class ParseXmlService
{
    public function modifyInputDataFromXmlObject(SimpleXMLElement $xmlObject): array
    {
        $data = [];
        $i = 0;

        foreach ($xmlObject as $item) {
            if (!empty($item->dealer->__toString())) {
                $dealer = Dealer::firstOrCreate([
                    'id' => $item->dealer['id'],
                    'title' => $item->dealer->__toString()
                ]);
            }
            if (!empty($item->category->__toString())) {
                $category = Category::firstOrCreate([
                    'id' => $item->category['id'],
                    'title' => $item->category->__toString()
                ]);
            }
            if (!empty($item->brand->__toString())) {
                $brand = Brand::firstOrCreate([
                    'id' => $item->brand['id'],
                    'title' => $item->brand->__toString()
                ]);
            }
            if (!empty($item->model->__toString())) {
                $modelCar = ModelCar::firstOrCreate([
                    'id' => $item->model['id'],
                    'title' => $item->model->__toString(),
                    'brand_id' => $brand->id
                ]);
            }
            if (!empty($item->generation->__toString())) {
                $generation = Generation::firstOrCreate([
                    'id' => $item->generation['id'],
                    'title' => $item->generation->__toString(),
                    'model_car_id' => $modelCar->id
                ]);
            }
            if (!empty($item->bodyConfiguration->__toString())) {
                $bodyConfiguration = BodyConfiguration::firstOrCreate([
                    'id' => $item->bodyConfiguration['id'],
                    'title' => $item->bodyConfiguration->__toString()
                ]);
            }
            if (!empty($item->modification->__toString())) {
                $modification = Modification::firstOrCreate([
                    'id' => $item->modification['id'],
                    'title' => $item->modification->__toString(),
                    'model_car_id' => $modelCar->id
                ]);
            }

            $data[$i]['id'] = (int)$item->id->__toString();
            $data[$i]['dealer_id'] = $dealer->id ?? null;
            $data[$i]['category_id'] = $category->id ?? null;
            $data[$i]['brand_id'] = $brand->id ?? null;
            $data[$i]['model_car_id'] = $modelCar->id ?? null;
            $data[$i]['generation_id'] = $generation->id ?? null;
            $data[$i]['body_configuration_id'] = $bodyConfiguration->id ?? null;
            $data[$i]['modification_id'] = $modification->id ?? null;
            $data[$i]['uin'] = (int)$item->uin->__toString();
            $data[$i]['subcategory'] = $item->subcategory->__toString();
            $data[$i]['type'] = $item->type->__toString();
            $data[$i]['year'] = $item->year->__toString();
            $data[$i]['complectation'] = $item->complectation->__toString();
            $data[$i]['brand_complectation_code'] = $item->brandComplectationCode->__toString();
            $data[$i]['engine_type'] = $item->engineType->__toString();
            $data[$i]['engine_volume'] = $item->engineVolume->__toString();
            $data[$i]['engine_power'] = $item->enginePower->__toString();
            $data[$i]['body_type'] = $item->bodyType->__toString();
            $data[$i]['body_door_count'] = $item->bodyDoorCount->__toString();
            $data[$i]['body_color'] = $item->bodyColor->__toString();
            $data[$i]['body_color_metallic'] = (boolean)$item->bodyColorMetallic->__toString();
            $data[$i]['drive_type'] = $item->driveType->__toString();
            $data[$i]['gearbox_type'] = $item->gearboxType->__toString();
            $data[$i]['gearbox_gear_count'] = $item->gearboxGearCount->__toString();
            $data[$i]['steering_wheel'] = $item->steeringWheel->__toString();
            $data[$i]['mileage'] = $item->mileage->__toString();
            $data[$i]['mileageUnit'] = $item->mileageUnit->__toString();
            $data[$i]['availability'] = $item->availability->__toString();
            $data[$i]['price'] = $item->price->__toString();
            $data[$i]['special_offer'] = (boolean)$item->specialOffer->__toString();
            $data[$i]['special_offer_previous_price'] = (boolean)$item->specialOfferPreviousPrice->__toString();
            $data[$i]['tradein_discount'] = empty($item->tradeinDiscount->__toString()) ? null : $item->tradeinDiscount->__toString();
            $data[$i]['credit_discount'] = empty($item->creditDiscount->__toString()) ? null : $item->creditDiscount->__toString();
            $data[$i]['insurance_discount'] = empty($item->insuranceDiscount->__toString()) ? null : $item->insuranceDiscount->__toString();
            $data[$i]['max_discount'] = empty($item->maxDiscount->__toString()) ? null : $item->maxDiscount->__toString();
            $data[$i]['pts_type'] = $item->ptsType->__toString();
            $data[$i]['country'] = $item->country->__toString();
            $data[$i]['operating_time'] = $item->operatingTime->__toString();
            $data[$i]['eco_class'] = $item->ecoClass->__toString();
            $data[$i]['drive_wheel'] = $item->driveWheel->__toString();
            $data[$i]['axis_count'] = empty($item->axisCount->__toString()) ? null : $item->axisCount->__toString();
            $data[$i]['brake_type'] = $item->brakeType->__toString();
            $data[$i]['cabin_type'] = $item->cabinType->__toString();
            $data[$i]['maximum_permitted_mass'] = empty($item->maximumPermittedMass->__toString()) ? null : $item->maximumPermittedMass->__toString();
            $data[$i]['saddle_height'] = empty($item->saddleHeight->__toString()) ? null : $item->saddleHeight->__toString();
            $data[$i]['cabin_suspension'] = $item->cabinSuspension->__toString();
            $data[$i]['chassis_suspension'] = $item->chassisSuspension->__toString();
            $data[$i]['length'] = empty($item->length->__toString()) ? null : $item->length->__toString();
            $data[$i]['width'] = empty($item->width->__toString()) ? null : $item->width->__toString();
            $data[$i]['body_volume'] = empty($item->bodyVolume->__toString()) ? null : $item->bodyVolume->__toString();
            $data[$i]['bucket_volume'] = empty($item->bucketVolume->__toString()) ? null : $item->bucketVolume->__toString();
            $data[$i]['traction_class'] = $item->tractionClass->__toString();
            $data[$i]['refrigerator_class'] = $item->refrigeratorClass->__toString();
            $data[$i]['crane_arrow_radius'] = empty($item->craneArrowRadius->__toString()) ? null : $item->craneArrowRadius->__toString();
            $data[$i]['crane_arrow_length'] = empty($item->craneArrowLength->__toString()) ? null : $item->craneArrowLength->__toString();
            $data[$i]['crane_arrow_payload'] = empty($item->craneArrowPayload->__toString()) ? null : $item->craneArrowPayload->__toString();
            $data[$i]['load_height'] = empty($item->loadHeight->__toString()) ? null : $item->loadHeight->__toString();
            $data[$i]['photo_count'] = empty($item->photoCount->__toString()) ? null : $item->photoCount->__toString();
            $data[$i]['description'] = $item->description->__toString();
            $data[$i]['owners_count'] = empty($item->ownersCount->__toString()) ? null : $item->ownersCount->__toString();
            $data[$i]['vehicle_condition'] = $item->vehicleCondition->__toString();
            $data[$i]['brand_color_code'] = $item->brandColorCode->__toString();
            $data[$i]['brand_interior_code'] = $item->brandInteriorCode->__toString();
            $data[$i]['certification_program'] = $item->certificationProgram->__toString();
            $data[$i]['acquisition_source'] = $item->acquisitionSource->__toString();
            $data[$i]['acquisition_date'] = $item->acquisitionDate->__toString();
            $data[$i]['manufacture_date'] = empty($item->manufactureDate) ? null : new DateTime($item->manufactureDate);
            $data[$i]['equipments'] = $item->equipment[0];
            $i++;
        }

        return $data;
    }
}