<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->string('uin')->nullable();
            $table->foreignId('dealer_id')->nullable()->references('id')->on('dealers');
            $table->foreignId('category_id')->nullable()->references('id')->on('categories');
            $table->foreignId('brand_id')->nullable()->references('id')->on('brands');
            $table->foreignId('model_car_id')->nullable()->references('id')->on('model_cars');
            $table->foreignId('generation_id')->nullable()->references('id')->on('generations');
            $table->foreignId('body_configuration_id')->nullable()->references('id')->on('body_configurations');
            $table->foreignId('modification_id')->nullable()->references('id')->on('modifications');
            $table->string('subcategory')->nullable();
            $table->string('complectation')->nullable();
            $table->string('brand_complectation_code')->nullable();
            $table->string('type');
            $table->integer('year');
            $table->enum('engine_type', ['petrol', 'diesel', 'hybrid', 'electric']);
            $table->integer('engine_volume');
            $table->integer('engine_power');
            $table->string('body_type');
            $table->enum('body_door_count', [range(2, 5)]);
            $table->string('body_color');
            $table->boolean('body_color_metallic');
            $table->enum('drive_type', ['front', 'full_4wd', 'rear']);
            $table->enum('gearbox_type', ['automatic', 'robotized', 'variator', 'manual']);
            $table->integer('gearbox_gear_count')->nullable();
            $table->enum('steering_wheel', ['left', 'right']);
            $table->enum('pts_type', ['original', 'duplicate', 'electronic']);
            $table->integer('mileage');
            $table->string('mileageUnit');
            $table->string('availability');
            $table->integer('price');
            $table->boolean('special_offer');
            $table->boolean('special_offer_previous_price')->nullable();
            $table->integer('tradein_discount')->nullable();
            $table->integer('credit_discount')->nullable();
            $table->integer('insurance_discount')->nullable();
            $table->integer('max_discount')->nullable();
            $table->string('country')->nullable();
            $table->string('operating_time')->nullable();
            $table->string('eco_class')->nullable();
            $table->string('drive_wheel')->nullable();
            $table->integer('axis_count')->nullable();
            $table->integer('maximum_permitted_mass')->nullable();
            $table->integer('saddle_height')->nullable();
            $table->integer('length')->nullable();
            $table->integer('width')->nullable();
            $table->integer('body_volume')->nullable();
            $table->integer('bucket_volume')->nullable();
            $table->integer('crane_arrow_radius')->nullable();
            $table->integer('crane_arrow_length')->nullable();
            $table->integer('crane_arrow_payload')->nullable();
            $table->integer('load_height')->nullable();
            $table->integer('photo_count')->nullable();
            $table->integer('owners_count')->nullable();
            $table->string('brake_type')->nullable();
            $table->string('cabin_type')->nullable();
            $table->string('cabin_suspension')->nullable();
            $table->string('chassis_suspension')->nullable();
            $table->string('traction_class')->nullable();
            $table->string('refrigerator_class')->nullable();
            $table->longText('description')->nullable();
            $table->string('vehicle_condition')->nullable();
            $table->string('brand_color_code')->nullable();
            $table->string('brand_interior_code')->nullable();
            $table->string('certification_program')->nullable();
            $table->string('acquisition_source')->nullable();
            $table->timestamp('acquisition_date')->nullable();
            $table->timestamp('manufacture_date')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
};
